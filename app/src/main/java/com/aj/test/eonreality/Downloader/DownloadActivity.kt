package com.aj.test.eonreality.Downloader

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import com.aj.test.eonreality.Downloader.database.models.Document
import com.aj.test.eonreality.Downloader.utils.Functions
import com.androidnetworking.AndroidNetworking

import kotlinx.android.synthetic.main.activity_download.*
import kotlinx.android.synthetic.main.nav_bar.view.*
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.DownloadListener


class DownloadActivity : AppCompatActivity() {

    var document: Document? = Document(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_download)
        setSupportActionBar(nav_bar.toolbar)
        AndroidNetworking.initialize(applicationContext)

        val spinnerContentTypes: Spinner = findViewById(R.id.spinnerContentTypes)
        ArrayAdapter.createFromResource(
            this@DownloadActivity,
            R.array.content_types_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinnerContentTypes.adapter = adapter
        }

        buttonDownload.setOnClickListener {
            var validated = true
            val urlString = textUrl.text.toString()
            when {
                urlString.isEmpty() -> {
                    validated = false
                    Functions.toast(this@DownloadActivity, getString(R.string.validated_required), getString(R.string.validated_url))
                }
                !Functions.verifyUrl(urlString) -> {
                    validated = false
                    Functions.toast(this@DownloadActivity, getString(R.string.validated_invalid), getString(R.string.validated_invalid_url))
                }
                textDesc.text.isEmpty() -> {
                    validated = false
                    Functions.toast(this@DownloadActivity, getString(R.string.validated_required), getString(R.string.validated_desc))
                }
            }
            if (validated) {
                document = Document(this@DownloadActivity)
                document!!.url = urlString
                document!!.desc = textDesc.text.toString()
                document!!.contentType = spinnerContentTypes.selectedItem.toString()
                document!!.createdAt = System.currentTimeMillis()

                try {
                    buttonDownload.isEnabled = false
                    val downloadPath = Functions.downloadPath()
                    val fileName = Functions.getFileName(urlString)
                    AndroidNetworking.download(urlString, downloadPath, fileName)
                        .setPriority(Priority.HIGH)
                        .build()
                        .setDownloadProgressListener { bytesDownloaded, totalBytes ->
                            val pr: Double = (bytesDownloaded.toDouble()/totalBytes.toDouble())*100.00
                            progressBar.progress = pr.toInt()
                        }
                        .startDownload(object : DownloadListener {

                            override fun onDownloadComplete() {
                                progressBar.progress = 0
                                buttonDownload.isEnabled = true

                                document!!.path =  "$downloadPath/$fileName"
                                document!!.insert()

                                Functions.toast(this@DownloadActivity, getString(R.string.success_title), getString(R.string.document_download_success))

                                finish()
                            }

                            override fun onError(error: ANError) {
                                progressBar.progress = 0
                                buttonDownload.isEnabled = true

                                Functions.toast(this@DownloadActivity, getString(R.string.error_title), getString(R.string.document_download_error))
                            }

                        })
                } catch (e: Exception) {
                    Functions.toast(this, getString(R.string.error_title), e.message!!)
                }
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_close -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}
