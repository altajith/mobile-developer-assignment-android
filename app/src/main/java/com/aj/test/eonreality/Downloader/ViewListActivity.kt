package com.aj.test.eonreality.Downloader

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.aj.test.eonreality.Downloader.adapters.DocumentListAdapter
import com.aj.test.eonreality.Downloader.database.models.Document

import kotlinx.android.synthetic.main.activity_view_list.*
import kotlinx.android.synthetic.main.nav_bar.view.*

class ViewListActivity : AppCompatActivity() {

    private var documentsListArray: ArrayList<Document>? = null
    private var documentListAdapter: DocumentListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_list)
        setSupportActionBar(nav_bar.toolbar)

        documentsListArray = Document(this).getAll()

        documentListAdapter = DocumentListAdapter(documentsListArray!!, this)
        listDownloadList!!.adapter = this.documentListAdapter
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_close -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
