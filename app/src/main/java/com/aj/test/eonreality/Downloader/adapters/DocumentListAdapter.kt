package com.aj.test.eonreality.Downloader.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.TextView
import com.aj.test.eonreality.Downloader.R
import com.aj.test.eonreality.Downloader.database.models.Document
import com.aj.test.eonreality.Downloader.utils.Functions
import java.io.File
import java.util.*
import android.content.Intent
import androidx.core.content.FileProvider.getUriForFile


class DocumentListAdapter(private val dataSet: ArrayList<Document>, mContext: Context) :
    ArrayAdapter<Document>(mContext, R.layout.document_list_item, dataSet), View.OnClickListener {

    override fun onClick(p0: View?) {
    }

    // View lookup cache
    private class ViewHolder {
        internal var labelFileName: TextView? = null
        internal var labelFileType: TextView? = null
        internal var labelFileDesc: TextView? = null
        internal var buttonFileView: Button? = null
    }

    override fun getCount(): Int {
        return dataSet.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        // Get the data item for this position
        val dataModel = getItem(position)
        // Check if an existing view is being reused, otherwise inflate the view
        val viewHolder: ViewHolder // view lookup cache stored in tag

        if (convertView == null) {

            viewHolder = ViewHolder()
            val inflater = LayoutInflater.from(context)
            convertView = inflater.inflate(R.layout.document_list_item, parent, false)
            viewHolder.labelFileName = convertView!!.findViewById(R.id.labelFileName)
            viewHolder.labelFileType = convertView.findViewById(R.id.labelFileType)
            viewHolder.labelFileDesc = convertView.findViewById(R.id.labelFileDesc)
            viewHolder.buttonFileView = convertView.findViewById(R.id.buttonFileView)

            convertView.tag = viewHolder
        } else {
            viewHolder = convertView.tag as ViewHolder
        }

        viewHolder.labelFileName!!.text = File(dataModel!!.path!!).name
        viewHolder.labelFileType!!.text = dataModel.contentType
        viewHolder.labelFileDesc!!.text = dataModel.desc

        viewHolder.buttonFileView!!.setOnClickListener {
            try {
                val uri = getUriForFile(
                    context,
                    "${context.packageName}.fileprovider",
                    File(dataModel.path!!)
                )
                val mime = context.contentResolver.getType(uri)

                val intent = Intent()
                intent.action = Intent.ACTION_VIEW
                intent.setDataAndType(uri, mime)
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                context.startActivity(intent)
            } catch (e: Exception) {
                Functions.toast(context, context.getString(R.string.error_title), e.message!!)
            }
        }

        convertView.layoutParams

        return convertView
    }
}
