package com.aj.test.eonreality.Downloader.database


import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import android.util.Log

class Database(context: Context) : SQLiteOpenHelper(context,
    DATABASE_NAME, null,
    DATABASE_VERSION
) {

    object DocumentFields : BaseColumns {
        const val TABLE_NAME = "documents"
        const val COLUMN_URL = "url"
        const val COLUMN_PATH = "path"
        const val COLUMN_DESC = "desc"
        const val COLUMN_CONTENT_TYPE = "content_type"
        const val COLUMN_CREATED_AT = "created_at"
    }

    private val sqlCreatDocumentsTable =
        "CREATE TABLE ${DocumentFields.TABLE_NAME} (" +
                "${BaseColumns._ID} INTEGER PRIMARY KEY," +
                "${DocumentFields.COLUMN_URL} TEXT,"  +
                "${DocumentFields.COLUMN_PATH} TEXT," +
                "${DocumentFields.COLUMN_DESC} TEXT," +
                "${DocumentFields.COLUMN_CONTENT_TYPE} TEXT," +
                "${DocumentFields.COLUMN_CREATED_AT} REAL);"

    private val sqlDeleteDocuments = "DROP TABLE IF EXISTS ${DocumentFields.TABLE_NAME};"

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(sqlCreatDocumentsTable)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL(sqlDeleteDocuments)
        onCreate(db)
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }

    companion object {
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "Downloader.db"
    }

}