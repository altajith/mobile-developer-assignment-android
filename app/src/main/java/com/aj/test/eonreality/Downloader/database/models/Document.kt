package com.aj.test.eonreality.Downloader.database.models

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import androidx.core.database.getLongOrNull
import androidx.core.database.getStringOrNull
import com.aj.test.eonreality.Downloader.database.Database


class Document(context: Context?) {

    private var database: Database? = null

    var id: String? = ""
    var url: String? = ""
    var path: String? = ""
    var desc: String? = ""
    var contentType: String? = ""
    var createdAt: Long? = 0

    init {
        if (context != null) {
            this.database = Database(context)
        }
    }

    @SuppressLint("Recycle")
    fun getAll(): ArrayList<Document> {
        val docArray: ArrayList<Document> = ArrayList()
        val dbR = this.database!!.readableDatabase
        val entries: Cursor = dbR.rawQuery("SELECT * FROM ${Database.DocumentFields.TABLE_NAME} ORDER BY created_at DESC", null)
        if (entries.count > 0) {
            while (entries.moveToNext()) {
                val newDoc = Document(null)
                newDoc.id = entries.getStringOrNull(0)
                newDoc.url = entries.getStringOrNull(1)
                newDoc.path = entries.getStringOrNull(2)
                newDoc.desc = entries.getStringOrNull(3)
                newDoc.contentType = entries.getStringOrNull(4)
                newDoc.createdAt = entries.getLongOrNull(5)
                docArray.add(newDoc)
            }
        }
        return docArray
    }

    @SuppressLint("Recycle")
    fun insert() {
        val values = ContentValues()
        values.put(Database.DocumentFields.COLUMN_URL, this.url)
        values.put(Database.DocumentFields.COLUMN_PATH, this.path)
        values.put(Database.DocumentFields.COLUMN_DESC, this.desc)
        values.put(Database.DocumentFields.COLUMN_CONTENT_TYPE, this.contentType)
        values.put(Database.DocumentFields.COLUMN_CREATED_AT, this.createdAt)

        val dbW = this.database!!.writableDatabase
        val dbR = this.database!!.readableDatabase
        val entries: Cursor = dbR.rawQuery("SELECT * FROM ${Database.DocumentFields.TABLE_NAME} WHERE _id = ?",  arrayOf(this.id))
        if (entries.count == 0) {
            dbW.insertOrThrow(Database.DocumentFields.TABLE_NAME, null,values)
        }
        dbR.close()
        dbW.close()
    }

}