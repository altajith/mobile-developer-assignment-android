package com.aj.test.eonreality.Downloader.utils

import android.content.Context
import android.os.Environment
import android.util.Patterns
import android.widget.Toast
import java.net.URL
import com.aj.test.eonreality.Downloader.BuildConfig
import java.io.File


class Functions {

    companion object {
        fun toast(ctx: Context, title: String, message: String) {
            Toast.makeText(ctx,
                "$title: $message", Toast.LENGTH_SHORT).show()
        }
        fun verifyUrl(url: String): Boolean {
            return Patterns.WEB_URL.matcher(url).matches()
        }
        fun downloadPath(): String {
            val path = Environment.getExternalStorageDirectory().path
            return "$path/{${BuildConfig.APPLICATION_ID}}"
        }
        fun getFileName(url: String): String {
            return File(URL(url).path).name
        }
    }

}